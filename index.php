<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Coold Blooded : " . $sheep->cold_blooded . "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Coold Blooded : " . $kodok->cold_blooded . "<br>";
echo "<br>" . $kodok->Jump();

$sungokong = new Ape("kera sakti");
echo "<br> Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Coold Blooded : " . $sungokong->cold_blooded . "<br>";
echo $sungokong->Yell();
